package utilities;

import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class DateUtility {

    public static String getFullMonthName(LocalDate date) {
        return date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(new Locale("ru")));
    }

    public static String toPointDelimiter(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }

    public static String getMonthName(LocalDate date) {
        String dateFormatted
                = date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(new Locale("ru")));
        String monthName = dateFormatted.split(" ")[1];
        return monthName;
    }

    public static LocalDateTime fileTimeToLocalDateTime(FileTime time) {
        long millis = time.toMillis();
        LocalDateTime dateTime = Instant.ofEpochMilli(millis).atZone(ZoneId.systemDefault()).toLocalDateTime();
        return dateTime;
    }
}
