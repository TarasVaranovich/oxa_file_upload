package utilities;

import model.Contract;
import model.Passport;
import model.Person;
import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.requireNonNull;


public class FileUtility {

    /**
     * Method fills document template.
     *
     * @param templateName
     * @param contractPath
     * @param contract
     * @return relative path to filled template.
     * @throws IOException
     * @throws InvalidFormatException
     */
    public void createContract(String templateName, String contractPath, Contract contract, Type type) throws Exception {
        FileInputStream inputStream = new FileInputStream(templateName);
        DocTypeHandler handler = resolveHandler(type);

        POIXMLDocument document = handler.readFile(inputStream);

        document = handler.fillTag(document, "{contractNumber}", contract.getNumber());
        document = handler.fillTag(document, "{contractStartDate}", DateUtility.getFullMonthName(contract.getStartDate()));
        document = handler.fillTag(document, "{contractEndDate}", DateUtility.getFullMonthName(contract.getEndDate()));
        document = handler.fillTag(document, "{agentName}", getGenitiveCaseFullName(contract.getAgent()));
        document = handler.fillTag(document, "{agentAddress}", contract.getAgent().getAddress().getFullAddress());
        document = handler.fillTag(document, "{agentPassport}", contract.getAgent().getPassport().getFullNumber());
        document = handler.fillTag(document, "{agentPassportGiver}", getPassportDeliveryInfo(contract.getAgent().getPassport()));
        document = handler.fillTag(document, "{renterName}", getGenitiveCaseFullName(contract.getRenter()));
        document = handler.fillTag(document, "{renterAddress}", contract.getRenter().getAddress().getFullAddress());
        document = handler.fillTag(document, "{renterPassport}", contract.getRenter().getPassport().getFullNumber());
        document = handler.fillTag(document, "{renterPassportGiver}", getPassportDeliveryInfo(contract.getRenter().getPassport()));

        inputStream.close();

        FileOutputStream outputStream = new FileOutputStream(String.format("%s%s.%s", contractPath, contract.getNumber(), type.toString().toLowerCase()));
        document.write(outputStream);
        outputStream.close();
    }

    public void clearDirectory(String path) {
        List<File> files = Arrays.asList(requireNonNull(new File(path).listFiles(File::isFile)));
        files.parallelStream().forEach(File::delete);
        List<File> directories = Arrays.asList(requireNonNull(new File(path).listFiles(File::isDirectory)));
        directories.parallelStream().forEach(this::deleteDirectory);
    }

    //this method can be used in scheduler
    public void clearDirectoryOlderThan(String path, LocalDateTime dateTime) throws IOException {
        File[] files = new File(path).listFiles(File::isFile);
        for (File file : files) {
            clearDirectoryOlderThan(dateTime, file);
        }

        File[] directories = new File(path).listFiles(File::isDirectory);
        for (File file : directories) {
            deleteDirectoryOlderThan(dateTime, file);
        }

    }

    private void clearDirectoryOlderThan(LocalDateTime dateTime, File file) throws IOException {
        BasicFileAttributes attributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
        LocalDateTime fileTime = DateUtility.fileTimeToLocalDateTime(attributes.creationTime());
        if (dateTime.isAfter(fileTime)) {
            file.delete();
        }
    }

    private void deleteDirectoryOlderThan(LocalDateTime dateTime, File file) throws IOException {
        BasicFileAttributes attributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
        LocalDateTime fileTime = DateUtility.fileTimeToLocalDateTime(attributes.creationTime());
        if (dateTime.isAfter(fileTime)) {
            deleteDirectory(file);
        }
    }

    private void deleteDirectory(File folder) {
        File[] files = folder.listFiles();
        if (files != null) { //some JVMs return null for empty dirs
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                } else {
                    file.delete();
                }
            }
        }
        folder.delete();
    }

    private DocTypeHandler resolveHandler(Type type) throws Exception {
        switch (type) {
            case DOCX:
                return new DocxHandler();
            case XLSX:
                return new XlsxHandler();
            default:
                //TODO: replace on more appropriate specific exception type.
                throw new Exception("Unknown document type.");
        }
    }

    private String getGenitiveCaseFullName(Person person) {
        //TODO: concern genitive cases for different names (feminine as example).
        return person.getSurname() + "а " + person.getName() + "а " + person.getPatronymic() + "а";
    }

    private String getPassportDeliveryInfo(Passport passport) {
        return passport.getGiver().replace("ий", "им") + " " +
                DateUtility.toPointDelimiter(passport.getDate());
    }
}
