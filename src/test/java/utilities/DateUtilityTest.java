package utilities;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;

@RunWith(SpringJUnit4ClassRunner.class)
public class DateUtilityTest {

    @Test
    public void toFullMonthNameTest() {
        LocalDate date = LocalDate.of(2014, 5, 6);
        Assert.assertEquals(DateUtility.getFullMonthName(date), "6 мая 2014 г.");
    }

    @Test
    public void toPointDelimiterTest() {
        LocalDate date = LocalDate.of(2014, 5, 6);
        System.out.println(DateUtility.toPointDelimiter(date));
        Assert.assertEquals(DateUtility.toPointDelimiter(date), "06.05.2014");
    }
}
