package model;

import java.time.LocalDate;
import java.util.Objects;

public class Passport {
    private String series;
    private Integer number;
    private String giver;
    private LocalDate date;

    public Passport() {

    }

    public Passport(String series, Integer number, String giver, LocalDate date) {
        this.series = series;
        this.number = number;
        this.giver = giver;
        this.date = date;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getGiver() {
        return giver;
    }

    public void setGiver(String giver) {
        this.giver = giver;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getFullNumber() {
        return this.getSeries() + this.getNumber().toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passport passport = (Passport) o;
        return series.equals(passport.series) &&
                number.equals(passport.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(series, number);
    }

    @Override
    public String toString() {
        return "Passport{" +
                "series='" + series + '\'' +
                ", number=" + number +
                ", giver='" + giver + '\'' +
                ", date=" + date +
                '}';
    }
}
