package model;

import java.util.Objects;

public class Address {
    private String city;
    private String street;
    private Integer buildingNumber;
    private Integer pavilionNumber;
    private Integer apartmentNumber;

    public Address() {

    }

    public Address(String city, String street, Integer buildingNumber, Integer apartmentNumber) {
        this.city = city;
        this.street = street;
        this.buildingNumber = buildingNumber;
        this.apartmentNumber = apartmentNumber;
    }

    public Address pavilionNumber(Integer pavilionNumber) {
        this.pavilionNumber = pavilionNumber;
        return this;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(Integer buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public Integer getPavilionNumber() {
        return pavilionNumber;
    }

    public void setPavilionNumber(Integer pavilionNumber) {
        this.pavilionNumber = pavilionNumber;
    }

    public Integer getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(Integer apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public String getFullAddress() {
        String pavilionNumber = this.pavilionNumber == null ? "" : "/" + this.getPavilionNumber();
        return "г. " + this.city + ", " + this.street + " " + this.getBuildingNumber() + pavilionNumber
                + "-" + this.getApartmentNumber();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return city.equals(address.city) &&
                street.equals(address.street) &&
                buildingNumber.equals(address.buildingNumber) &&
                pavilionNumber.equals(address.pavilionNumber) &&
                apartmentNumber.equals(address.apartmentNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, street, buildingNumber, pavilionNumber, apartmentNumber);
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", buildingNumber=" + buildingNumber +
                ", pavilionNumber=" + pavilionNumber +
                ", apartmentNumber=" + apartmentNumber +
                '}';
    }
}
