package utilities;

import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.FileInputStream;
import java.io.IOException;

public class DocxHandler implements DocTypeHandler {

    @Override
    public POIXMLDocument fillTag(POIXMLDocument document, String tag, String value) {
        XWPFDocument docxDocument = (XWPFDocument) document;
        docxDocument.getParagraphs().parallelStream().forEach(paragraph -> fillTagDocx(paragraph, tag, value));
        return document;
    }

    @Override
    public XWPFDocument readFile(FileInputStream inputStream) throws IOException, InvalidFormatException {
        return new XWPFDocument(OPCPackage.open(inputStream));
    }

    private XWPFParagraph fillTagDocx(XWPFParagraph paragraph, String tagName, String value) {
        if (paragraph.getText().contains(tagName)) {
            paragraph.getRuns().parallelStream().forEach(run -> fillTagDocx(run, tagName, value));
        }
        return paragraph;
    }

    private XWPFRun fillTagDocx(XWPFRun run, String tagName, String value) {
        if (run.text().contains(tagName)) {
            String text = run.text().replace(tagName, value);
            run.setText(text, 0);
        }
        return run;
    }
}
