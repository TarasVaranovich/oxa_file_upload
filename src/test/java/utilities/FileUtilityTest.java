package utilities;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static utilities.DataGenerator.buildContract;

@RunWith(SpringJUnit4ClassRunner.class)
public class FileUtilityTest {

    private static final String DOCX_TEMPLATE_NAME = ".\\src\\main\\resources\\templates\\rent_contract_template.docx";
    private static final String DOCX_CONTRACT_DIR = ".\\src\\main\\resources\\public\\contracts\\";

    private static final String XLSX_TEMPLATE_NAME = ".\\src\\main\\resources\\templates\\rent_contract_template.xlsx";
    private static final String XLSX_CONTRACT_DIR = ".\\src\\main\\resources\\public\\contracts\\";

    private FileUtility fileUtility = new FileUtility();

    @Test
    public void fillDocxTemplateTest() throws Exception {
        fileUtility.createContract(DOCX_TEMPLATE_NAME, DOCX_CONTRACT_DIR, buildContract(), Type.DOCX);
    }

    @Test
    public void fillXlsxTemplateTest() throws Exception {
        fileUtility.createContract(XLSX_TEMPLATE_NAME, XLSX_CONTRACT_DIR, buildContract(), Type.XLSX);
    }

    @Test
    public void  clearDirectoryTest(){
        fileUtility.clearDirectory(DOCX_CONTRACT_DIR);
    }
}
