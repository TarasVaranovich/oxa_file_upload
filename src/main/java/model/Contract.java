package model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Class represents rent contract.
 */
public class Contract {
    private LocalDate startDate;
    private LocalDate endDate;
    private String number;
    private Person agent;
    private Person renter;

    public Contract(LocalDate startDate, LocalDate endDate, String number) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.number = number;
    }

    public Contract agent(Person agent) {
        this.agent = agent;
        return this;
    }

    public Contract renter(Person renter) {
        this.renter = renter;
        return this;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Person getAgent() {
        return agent;
    }

    public void setAgent(Person agent) {
        this.agent = agent;
    }

    public Person getRenter() {
        return renter;
    }

    public void setRenter(Person renter) {
        this.renter = renter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contract contract = (Contract) o;
        return Objects.equals(number, contract.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, startDate);
    }

    @Override
    public String toString() {
        return "Contract{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", number='" + number + '\'' +
                ", agent=" + agent +
                ", renter=" + renter +
                '}';
    }
}
