package accesspoint;

import model.Contract;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import utilities.DataGenerator;
import utilities.FileUtility;
import utilities.Type;

import javax.servlet.ServletException;
import java.io.File;
import java.util.UUID;
import java.util.logging.Logger;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class FileController {
    private static final String DOCX_TEMPLATE_NAME = ".\\src\\main\\resources\\templates\\rent_contract_template.docx";
    private static final String DOCX_CONTRACT_DIR = ".\\src\\main\\resources\\public\\contracts\\";
    private static final String WEB_DIR_PATH = "http://localhost:8080/contracts/";

    private Logger logger = Logger.getLogger(FileController.class.getSimpleName());

    @RequestMapping(value = "/file", method = POST)
    public String getUserData(@RequestBody String jsonString) throws ServletException {
        DataGenerator.buildContract();
        FileUtility fileUtility = new FileUtility();
        try {
            Contract contract = DataGenerator.buildContract();
            Type type = Type.DOCX;

            UUID uuid = UUID.randomUUID();
            String randomDir = uuid.toString() +"\\";
            File dir = new File(DOCX_CONTRACT_DIR + randomDir);
            dir.mkdir();

            fileUtility.createContract(DOCX_TEMPLATE_NAME, DOCX_CONTRACT_DIR + randomDir, DataGenerator.buildContract(), type);
            return WEB_DIR_PATH + randomDir + "/" + contract.getNumber() + "." + type.toString().toLowerCase();
        } catch (Exception e) {
            //TODO: replace with normal exception or error page.
            e.printStackTrace();
            throw new ServletException("Problems in creating contract.");
        }
    }
}
