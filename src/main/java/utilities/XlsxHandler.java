package utilities;

import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;

public class XlsxHandler implements DocTypeHandler {

    @Override
    public POIXMLDocument fillTag(POIXMLDocument document, String tag, String value) {
        XSSFWorkbook xlsxDocument = (XSSFWorkbook) document;
        XSSFSheet sheet = xlsxDocument.getSheet("rent_contract_template");
        for (Row row : sheet) {
            for (Cell cell : row) {
                String text = cell.getRichStringCellValue().getString();
                text = text.replace(tag, value);
                cell.setCellValue(text);
            }
        }
        return document;
    }

    @Override
    public XSSFWorkbook readFile(FileInputStream inputStream) throws IOException, InvalidFormatException {
        return new XSSFWorkbook(OPCPackage.open(inputStream));
    }
}
