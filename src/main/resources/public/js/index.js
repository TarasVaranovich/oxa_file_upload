var nameInput = document.getElementById("userName");
var surnameInput = document.getElementById("userSurname");
var loginBtn = document.getElementById("getFileBtn");
var downloadRef = document.getElementById("downloadRef");

loginBtn.addEventListener("click", function (event) {
    var userData = new Object();
    userData.userName = nameInput.value;
    userData.userSurname = surnameInput.value;

    var userData = JSON.stringify(userData);
    var xhr = new XMLHttpRequest();
    xhr.open("post", "/file");
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(userData);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                downloadRef.href = xhr.responseText;
                downloadRef.style.display = "block";
            }
        }
    };
});
