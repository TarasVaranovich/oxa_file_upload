package model;

import java.util.Objects;

public class Person {
    private String name;
    private String surname;
    private String patronymic;
    private Passport passport;
    private Address address;

    public Person() {

    }

    public Person(String surname, String name, String patronymic, Passport passport, Address address) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.passport = passport;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Passport getPassport() {
        return passport;
    }

    public void setPassport(Passport passport) {
        this.passport = passport;
    }

    public String getFullName() {
        return this.surname + " " + this.name + " " + this.patronymic;
    }

    public String getInitialsName() {
        return this.surname + " " + this.name.charAt(0) + "." + this.patronymic.charAt(0) + ".";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person customer = (Person) o;
        return name.equals(customer.name) &&
                surname.equals(customer.surname) &&
                patronymic.equals(customer.patronymic) &&
                passport.getFullNumber().equals(customer.passport.getFullNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic, passport);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", passport=" + passport +
                ", address=" + address +
                '}';
    }
}
