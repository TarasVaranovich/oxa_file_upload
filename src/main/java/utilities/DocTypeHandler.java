package utilities;

import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.FileInputStream;
import java.io.IOException;

public interface DocTypeHandler {

    POIXMLDocument fillTag(POIXMLDocument document, String tag, String value);

    POIXMLDocument readFile(FileInputStream inputStream) throws IOException, InvalidFormatException;
}
