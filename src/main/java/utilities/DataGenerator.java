package utilities;

import model.Address;
import model.Contract;
import model.Passport;
import model.Person;

import java.time.LocalDate;

public class DataGenerator {

    public static Contract buildContract() {
        Passport agentPassport = new Passport("МР", 5667899, "Первомайский РОВД г. Минска",
                LocalDate.of(2013, 3, 15));
        Address agentAddress = new Address("Минск", "Независимости", 58, 1);
        Person agent = new Person("Сидоров", "Иван", "Иванович", agentPassport,
                agentAddress);

        Passport renterPassport = new Passport("МР", 1234567, "Партизанский РОВД г. Минска",
                LocalDate.of(2011, 6, 24));
        Address renterAddress = new Address("Минск", "Солтыса", 199, 45);
        Person renter = new Person("Иванов", "Иван", "Иванович", renterPassport,
                renterAddress);

        Contract contract = new Contract(LocalDate.of(2019, 5, 6),
                LocalDate.of(2019, 5, 10), "A123456").agent(agent).renter(renter);
        return contract;
    }
}
